//
//  CustomAlertViewController.h
//  rep
//
//  Created by Avances on 25/05/18.
//  Copyright © 2018 Avances. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomAlertViewControllerDelegate


- (void)customAlertdialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

@interface CustomAlertViewController : UIView<CustomAlertViewControllerDelegate>
@property (nonatomic, retain) UIView *parentView;    // The parent view this 'dialog' is attached to
@property (nonatomic, retain) UIView *dialogView;    // Dialog's container view
@property (nonatomic, retain) UIView *containerView; // Container within the dialog (place your ui elements here)
@property (nonatomic, retain) UIView *buttonView;    // Buttons on the bottom of the dialog

@property (nonatomic, assign) id<CustomAlertViewControllerDelegate> delegate;
@property (nonatomic, retain) NSArray *buttonColors;
@property (nonatomic, retain) NSArray *buttonTitles;
@property (nonatomic, assign) BOOL useMotionEffects;

@property (copy) void (^onButtonTouchUpInside)(CustomAlertViewController *alertView, int buttonIndex) ;

- (id)initWithParentView: (UIView *)_parentView;
- (id)init;

- (void)show;
- (void)close;


- (IBAction)customAlertdialogButtonTouchUpInside:(id)sender;

- (void)setOnButtonTouchUpInside:(void (^)(CustomAlertViewController *alertView, int buttonIndex))onButtonTouchUpInside;

+ (CustomAlertViewController *) alertWithTitle:(NSString *)title message:(NSString *)message;

@end
