//
//  ViewController.m
//  rep
//
//  Created by Avances on 25/05/18.
//  Copyright © 2018 Avances. All rights reserved.
//
/*
#import "ViewController.h"
#import "SalidaViewController.h"
//#import "AFNetworking/AFHTTPClient.h"
//#import "AFNetworking/AFHTTPRequestSerializer.h"
#import <AFNetworking/AFSecurityPolicy.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import "DGActivityIndicatorView/DGActivityIndicatorView.h"

@import Firebase;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtEmailClientRep; 
@property (weak, nonatomic) IBOutlet UIButton *btnEnviarEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogoISAREP;
@property (weak, nonatomic) IBOutlet UILabel *lblRegistrate;
@property (weak, nonatomic) IBOutlet UILabel *lblCorreo;
@property (weak, nonatomic) IBOutlet UIView *superview;

@end

@implementation ViewController

//NSString  *const BASE_URL = @"https://at-snm-dev-eus-api.azurewebsites.net/";

#ifndef NDEBUG
//    NSString  *const BASE_URL = @"https://at-snm-dev-eus-api.azurewebsites.net/";
    //NSString  *const BASE_URL = @"https://192.168.2.100/WebApiCert/";
    NSString  *const BASE_URL = @"http://200.48.76.243/Servicio/";
#else
    //NSString  *const BASE_URL = @"https://at-snm-dev-eus-api.azurewebsites.net/";
    NSString  *const BASE_URL = @"http://200.48.76.243/Servicio/";
#endif

- (void)viewDidLoad {
    _superview = self.view;
    [super viewDidLoad];
    [self layoutPantallalogin];
}

- (IBAction)txtEmail_endEdit:(id)sender {
    [self.txtEmailClientRep resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)validarEmailConString:(NSString*)email {
    //NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    //NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSString *emailRegex = @"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (void) showAlert:(NSString*) message {
    [self showAlert:message WithAction:nil];
}

- (void) showAlert:(NSString*) message WithAction:(void (^ __nullable)(UIAlertAction *action)) action {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alerta"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:action
 
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    
                                }
 
                                ];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnEnviarEmail_click:(id)sender {
    [[FIRMessaging messaging] subscribeToTopic:@"PushNotification de REP"
                                    completion:^(NSError * _Nullable error) {
                                        NSLog(@"Activacion de Push Notification de REP");
                                    }];
    NSString *UPN = [_txtEmailClientRep.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (UPN.length == 0) {
        [self showAlert:@"Por favor ingrese un e-mail"];
    } else  if([self validarEmailConString:UPN] == false) {
        [self showAlert:@"Por favor ingrese un e-mail con el formato correcto"];
    } else {
        NSString *topic = @"rep";
        NSString *tokenFCM = @"cAMvA4GW61I:APA91bHkvIFOrWJpcsWnhZzVJaGdl22DDQcVoBtFEz-uuCYigwQsjtJb7UKPDPolt5GcfJeq5P9oP9fDk_lVEdqouqhQoei0kr3cauPLkiqfVw2fzFNA8ESg_3cIgIBTBajD1tVbIZin";
        NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:tokenFCM, @"Token", UPN, @"Upn", topic, @"Topic", nil];
        
        [self sendEmail:jsonDictionary];
    }
}

- (void) sendEmail :(NSDictionary *) input {
    UIColor *iColor = [UIColor colorWithRed:0.04 green:0.21 blue:0.51 alpha:1.0];
    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:iColor size:50.0f];
    
    activityIndicatorView.backgroundColor = UIColor.blackColor;
    activityIndicatorView.frame = self.view.frame;
    activityIndicatorView.alpha = 0.8;
    
    [self.view addSubview:activityIndicatorView];
    
    [activityIndicatorView startAnimating];
    
    @try {
        //NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"PS2ChildCert" ofType:@"cer"];
        NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"PS2TestCert" ofType:@"cer"];
        NSData *certData = [NSData dataWithContentsOfFile:cerPath];
        NSSet *certSet = [[NSSet alloc] initWithObjects:certData, nil];
        
//        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate withPinnedCertificates:conjunto];
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
//        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        securityPolicy.allowInvalidCertificates = NO;
        securityPolicy.validatesDomainName = YES;
        securityPolicy.pinnedCertificates = certSet;


        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        //AFHTTPSessionManager *manager =  [AFHTTPSessionManager manager];
        //AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL] sessionConfiguration:configuration];
        
        //manager.securityPolicy = securityPolicy;
        //
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 10;
        //manager.securityPolicy.pinnedCertificates = certSet;
        //manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        //manager.requestSerializer  = [AFJSONRequestSerializer serializer];
        //manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
//        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
        
        NSString *ENLACE = @"api/dispositivo";//[BASE_URL stringByAppendingString:@"api/dispositivo"];
        
        NSLog(@"BASE: %@", BASE_URL);
        NSLog(@"ENLACE: %@", ENLACE);
        NSLog(@"INPUT: %@", input);
        NSLog(@"");
        NSLog(@"");
        NSLog(@"");
        NSLog(@"");
    
        NSURLSessionDataTask *task = [manager POST:ENLACE parameters:input progress:^(NSProgress * _Nonnull uploadProgress) {
            // DO NOTHING IN PROGRESS
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [activityIndicatorView stopAnimating];
            [activityIndicatorView removeFromSuperview];
            
            NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"Request Suceso, response '%@'", responseStr);
            
            _txtEmailClientRep.text = @"";
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:@"Los datos se enviaron correctamente"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"ACEPTAR"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            SalidaViewController *salida = [story instantiateViewControllerWithIdentifier:@"salidaViewController"];
                                            [self presentViewController:salida animated:YES completion:nil];
                                        }];
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"");
            NSLog(@"ERROR: %@", error);
            NSLog(@"");
            
            [activityIndicatorView stopAnimating];
            [activityIndicatorView removeFromSuperview];
            
            [self showAlert:@"Error conectando al servidor. Intentelo en breves momentos."];
        }];
        
        [task resume];
    } @catch (NSException *ex) {
        NSLog(@"ERROR: %@: %@", ex.name, ex.reason);
        
        [activityIndicatorView stopAnimating];
        [activityIndicatorView removeFromSuperview];

        [self showAlert:ex.reason];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification {
    // Assign new frame to your view
    //[self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
}

-(void)keyboardDidHide:(NSNotification *)notification {
    //[self.view setFrame:CGRectMake(0,0,320,460)];
}

-(void)layoutPantallalogin {
    _txtEmailClientRep.layer.cornerRadius = 8;
    _txtEmailClientRep.layer.borderWidth = 1;
    _txtEmailClientRep.clipsToBounds = YES;
    _txtEmailClientRep.layer.borderColor = UIColor.grayColor.CGColor;
    _txtEmailClientRep.delegate = self;
    
    //Boton enviar Email
    _btnEnviarEmail.layer.cornerRadius = 25;
    _btnEnviarEmail.clipsToBounds = YES;
    
#ifndef NDEBUG
    _txtEmailClientRep.text = @"yparedes@avances.com.pe";
#endif
}
@end
*/
