//
//  SalidaViewController.m
//  rep
//
//  Created by Avances on 25/05/18.
//  Copyright © 2018 Avances. All rights reserved.
//

#import "SalidaViewController.h"
#import "ViewController.h"

@interface SalidaViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnSalida;

@end

@implementation SalidaViewController
- (IBAction)btnSalida:(id)sender {
    
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ViewController *vc = [story instantiateViewControllerWithIdentifier:@"viewController"];
//    [self presentViewController:vc animated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _btnSalida.layer.cornerRadius = 25;
    _btnSalida.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
