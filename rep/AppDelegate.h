//
//  AppDelegate.h
//  rep
//
//  Created by Avances on 25/05/18.
//  Copyright © 2018 Avances. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


