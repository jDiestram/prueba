//
//  MainViewController.swift
//  rep
//
//  Created by Jesús Diestra on 24/07/18.
//  Copyright © 2018 Avances Tecnologicos All rights reserverd.
//

import UIKit
import AFNetworking
import EZLoadingActivity
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class MainViewController: UIViewController {
    @IBOutlet var txtMail: UITextField!
    @IBOutlet var btnSend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        txtMail.text = "jdiestra@gmail.com"
        //
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func SendMail(_ sender: Any) {
        EZLoadingActivity.show("Enviando", disableUI: true)
        print("hola")
        let txtValue = txtMail.text
        /*let url:String = "https://extranetprb.rep.com.pe/sendnot/api/dispositivo"
        let params: [String: String] = [
            "Token": "dKzDPOCQUV0:APA91bGgJM-Uxe0T-mLKBKhDWctCxTX8Jdc8bSNd7X_H52ulXmHJgsnAiX0AtHUoHDfSE_jwxGTQjPjf3VYDNo744CgEbImAbpPevtmuumOolqfTCwMQWOuxw_hMmdOKpUR9vg6-yIek",
            "Upn": "jdianderas@avances.com.pe"
            
        ]
        
        let headers: HTTPHeaders = ["Content-Type": "application/json"]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print("entra")
            print(response)
        }*/
        let parameters: Parameters = ["Token": "dKzDPOCQUV0:APA91bGgJM-Uxe0T-mLKBKhDWctCxTX8Jdc8bSNd7X_H52ulXmHJgsnAiX0AtHUoHDfSE_jwxGTQjPjf3VYDNo744CgEbImAbpPevtmuumOolqfTCwMQWOuxw_hMmdOKpUR9vg6-yIek", "Upn": "jdianderas@avances.com.pe"];
        let urlstring = "https://extranetprb.rep.com.pe/sendnot/api/dispositivo"
        let baseUrl = NSURL(string: urlstring)
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: baseUrl! as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        Alamofire.request(request).responseJSON { response in
            print(response)
            EZLoadingActivity.hide(true, animated: true)
            let alertController = UIAlertController(title: "Información", message: "Los datos se enviaron correctamente", preferredStyle: .alert)
            alertController.setValue(NSAttributedString(string: "Información", attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 18),NSForegroundColorAttributeName : UIColor.gray]), forKey: "attributedTitle")
            alertController.setValue(NSAttributedString(string: "Los datos se enviaron correctamente", attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 24, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.gray]), forKey: "attributedMessage")
            let action1 = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
                print("You've pressed default");
            }
            alertController.addAction(action1)
            
            alertController.view.tintColor = UIColor.white
            let subview = alertController.view.subviews.first! as UIView
            let alertContentView = subview.subviews.first! as UIView
            alertContentView.backgroundColor = UIColor.blue
            alertContentView.layer.cornerRadius = 15;
            
            
            self.present(alertController, animated: true, completion: nil)
            print(response.result.value)
            print(response.description)
            print(response.error)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
